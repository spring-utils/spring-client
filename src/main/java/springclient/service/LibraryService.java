package springclient.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import springclient.feign.LibraryClient;
import springclient.resources.Library;

@Service
public class LibraryService {

    @Autowired
    private LibraryClient libraryClient;

    public Library getLibrary() {
        return libraryClient.getLibrary();
    }
}
