package springclient;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class Launcher {
    public static void main(String[] args) {
        new SpringApplicationBuilder(Launcher.class).run(args);
    }
}
