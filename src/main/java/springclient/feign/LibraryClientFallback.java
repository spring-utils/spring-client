package springclient.feign;

import lombok.AllArgsConstructor;
import lombok.Data;
import springclient.resources.Library;

@Data
@AllArgsConstructor
public class LibraryClientFallback implements LibraryClient {

    private final Throwable cause;

    @Override
    public Library getLibrary() {
        return null;
    }
}
