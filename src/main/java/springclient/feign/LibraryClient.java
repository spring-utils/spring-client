package springclient.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

import springclient.resources.Library;

@FeignClient(value = "library", fallbackFactory = LibraryClientFallbackFactory.class)
public interface LibraryClient {

    public final static String SERVER_NAME = "library";
    public final static String LIBRARIES = "libraries";

    @GetMapping(LIBRARIES)
    Library getLibrary();
}
