package springclient.feign;

import org.springframework.stereotype.Component;

import feign.hystrix.FallbackFactory;
import lombok.extern.flogger.Flogger;

@Flogger
@Component
public class LibraryClientFallbackFactory implements FallbackFactory<LibraryClient> {
	@Override
	public LibraryClient create(Throwable cause) {
		log.atWarning().log("Unexpected connection problem.", cause);
		return new LibraryClientFallback(cause);
	}
}
