package springclient.scheduler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import lombok.extern.flogger.Flogger;
import springclient.resources.Library;
import springclient.service.LibraryService;

@Flogger
@Component
public class LibraryScheduler {

	@Autowired
	private LibraryService libraryService;

	@Scheduled(fixedDelayString = "${scheduler.library-intervall-millis}")
	public void run() {
		log.atInfo().log("Start Scheduler!");
		Library library = libraryService.getLibrary();
		log.atInfo().log("Result: " + library);
	}
}
