package springclient.config;

import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;

import feign.Logger;
import lombok.extern.flogger.Flogger;

@Flogger
@EnableCircuitBreaker
@EnableScheduling
@ConditionalOnProperty(value = "app.scheduling.enable", havingValue = "true", matchIfMissing = true)
@Configuration
public class ClientConfig {

	@Bean
	public Logger getLogger() {
		return new MyFeignLog();
	}

	@Bean
	public Logger.Level feignLoggerLevel() {
		return Logger.Level.BASIC;
	}

	// workaround to log all requests from this client
	private static class MyFeignLog extends feign.Logger {
		@Override
		protected void log(String configKey, String format, Object... args) {
			log.atInfo().log(String.format(methodTag(configKey) + format, args));
		}
	}
}
