package springclient.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.util.Assert.isNull;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import springclient.feign.LibraryClientFallback;
import springclient.feign.LibraryClientFallbackFactory;
import springclient.resources.Library;

@SpringBootTest
@RunWith(SpringRunner.class)
@TestPropertySource(properties = "app.scheduling.enable=false")
public class LibraryServiceTest {

    @Autowired
    private LibraryService libraryService;

    @Autowired
    private LibraryClientFallbackFactory clientFallback;

    @Test
    public void test_fallback() {
        Library library = libraryService.getLibrary();

        isNull(library, "Expect null from library-rest-fallback.");
        verify(clientFallback, atLeastOnce()).create(any());
    }

    /**
     * Get control of FallbackFactory
     */
    @TestConfiguration
    public static class TestConfig {
        @Bean
        @Primary
        public LibraryClientFallbackFactory mockSystemTypeDetector() {
            LibraryClientFallbackFactory fallbackFactory = Mockito.mock(LibraryClientFallbackFactory.class);
            when(fallbackFactory.create(any())).thenAnswer(invoce -> {
                Throwable throwable = invoce.getArgument(0);
                return new LibraryClientFallback(throwable);
            });
            return fallbackFactory;
        }
    }
}
